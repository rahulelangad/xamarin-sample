using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace InhomeApp.Droid
{
    public class LoginActivity: Activity
    {
        EditText _userNameText;
        EditText _passwordText;
        Button _loginButton;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            SetContentView(Resource.Layout.Main);
            _userNameText = FindViewById<EditText>(Resource.Id.PhonewordText);
            _passwordText = FindViewById<EditText>(Resource.Id.PasswordText);
            _loginButton = FindViewById<Button>(Resource.Id.LoginButton);
            _loginButton.Click += LoginButton_Click;
        }

        private void LoginButton_Click(object sender, System.EventArgs e)
        {

            TodoList _todoList = new TodoList();
            TodoRepository _todoRepository = new TodoRepository();
            _todoRepository.CreateAsync("test");
        }
    }
}