﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Provider;
using Java.IO;


namespace InhomeApp.Droid
{
    [Activity(Label = "InhomeApp", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        EditText _phoneNumberText;
        Button _translateButton;
        Button _callButton;
        string _translatedNumber;
        ImageView _imageView;
        private Button _gpsButton;
        private Button _smsButton;
        public static class App
        {
            public static File _file;
            public static File _dir;
            public static Bitmap bitmap;
        }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            if (IsThereAnAppToTakePictures())
            {
                CreateDirectoryForPictures();
                Button button = FindViewById<Button>(Resource.Id.myButton);
                _imageView = FindViewById<ImageView>(Resource.Id.imageView1);
                button.Click += TakeAPicture;
            }

            _phoneNumberText = FindViewById<EditText>(Resource.Id.PhonewordText);
            _translateButton = FindViewById<Button>(Resource.Id.TranslateButton);
            _callButton = FindViewById<Button>(Resource.Id.CallButton);
            _gpsButton = FindViewById<Button>(Resource.Id.GpsButton);
            _smsButton = FindViewById<Button>(Resource.Id.SMSButton);

            _translateButton.Click += TranslateButton_Click;
            _callButton.Click += CallButton_Click;
            _gpsButton.Click += GpsButton_Click;
            _smsButton.Click += SMSButton_Click;
        }
        private void GpsButton_Click(object sender, System.EventArgs e)
        {
            var gpsIntent = new Intent(this, typeof(GPSActivity));
            StartActivity(gpsIntent);
        }
        private void SMSButton_Click(object sender, System.EventArgs e)
        {
            var smsIntent = new Intent(this, typeof(SMSActivity));
            StartActivity(smsIntent);
        }
        private void TranslateButton_Click(object sender, System.EventArgs e)
        {
            _translatedNumber = PhonewordTranslator.ToNumber(_phoneNumberText.Text);
            if (string.IsNullOrWhiteSpace(_translatedNumber))
            {
                _callButton.Text = "Call";
                _callButton.Enabled = false;
            }
            else
            {
                _callButton.Text = "Call " + _translatedNumber;
                _callButton.Enabled = true;
            }
        }
        private void CallButton_Click(object sender, System.EventArgs e)
        {

            UtilityHelper utilityHelper = new UtilityHelper();
            utilityHelper.Call(_translatedNumber);
            //var callDialog = new AlertDialog.Builder(this);
            //callDialog.SetMessage("Call " + TranslatedNumber + "?");
            //callDialog.SetNeutralButton("Call", delegate
            //{
            //    var callIntent = new Intent(Intent.ActionCall);
            //    callIntent.SetData(Android.Net.Uri.Parse("tel:" + TranslatedNumber));
            //    StartActivity(callIntent);
            //});
            //callDialog.SetNegativeButton("Cancel", delegate { });
            //callDialog.Show();

        }
        private void CreateDirectoryForPictures()
        {
            App._dir = new File(
                Android.OS.Environment.GetExternalStoragePublicDirectory(
                    Android.OS.Environment.DirectoryPictures), "CameraAppDemo");
            if (!App._dir.Exists())
            {
                App._dir.Mkdirs();
            }
        }

        private bool IsThereAnAppToTakePictures()
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            IList<ResolveInfo> availableActivities =
                PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
        }
        private void TakeAPicture(object sender, EventArgs eventArgs)
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            App._file = new File(App._dir, String.Format("myPhoto_{0}.jpg", Guid.NewGuid()));
            intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(App._file));
            StartActivityForResult(intent, 0);
        }
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            // Make it available in the gallery

            Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
            Android.Net.Uri contentUri = Android.Net.Uri.FromFile(App._file);
            mediaScanIntent.SetData(contentUri);
            SendBroadcast(mediaScanIntent);

            // Display in ImageView. We will resize the bitmap to fit the display.
            // Loading the full sized image will consume to much memory
            // and cause the application to crash.

            int height = Resources.DisplayMetrics.HeightPixels;
            int width = _imageView.Height;
            App.bitmap = App._file.Path.LoadAndResizeBitmap(width, height);
            if (App.bitmap != null)
            {
                _imageView.SetImageBitmap(App.bitmap);
                App.bitmap = null;
            }

            // Dispose of the Java side bitmap.
            GC.Collect();
        }
    }
    public static class BitmapHelpers
    {
        public static Bitmap LoadAndResizeBitmap(this string fileName, int width, int height)
        {
            // First we get the the dimensions of the file on disk
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            BitmapFactory.DecodeFile(fileName, options);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            int outHeight = options.OutHeight;
            int outWidth = options.OutWidth;
            int inSampleSize = 1;

            if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
                                   ? outHeight / height
                                   : outWidth / width;
            }

            // Now we will load the image and have BitmapFactory resize it for us.
            options.InSampleSize = inSampleSize;
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(fileName, options);

            return resizedBitmap;
        }
    }
}


