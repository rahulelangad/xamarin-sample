using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Telephony;
using Android.Views;
using Android.Widget;

namespace InhomeApp.Droid
{
    [Activity(Label = "SMS", MainLauncher = false, Icon = "@drawable/icon")]
    public class SMSActivity : Activity
    {
        EditText _phoneNoText;
        EditText _smsContentText;
        Button _sendSmsButton;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.SMS);

            _phoneNoText = FindViewById<EditText>(Resource.Id.PhoneNoText);
            _smsContentText = FindViewById<EditText>(Resource.Id.SMSContentText);
            _sendSmsButton = FindViewById<Button>(Resource.Id.SendSMSButton);
            _sendSmsButton.Click += SendSMSButton_Click;
        }
        private void SendSMSButton_Click(object sender, System.EventArgs e)
        {
            /* SMS - defualt -
              string msg = SMSContentText.Text;
             var smsIntent = new Intent(Intent.ActionView);
             smsIntent.PutExtra("sms_body", msg);
             smsIntent.SetType("vnd.android-dir/mms-sms");
             StartActivity(smsIntent);


            string msg = SMSContentText.Text;
            string toNumber = PhoneNoText.Text;
            SmsManager smsManager = SmsManager.Default;
            smsManager.SendTextMessage(toNumber, null, msg, null, null);*/

            UtilityHelper utilityHelper = new UtilityHelper();
            utilityHelper.Sms(_phoneNoText.Text, _smsContentText.Text);
        }
    }
}