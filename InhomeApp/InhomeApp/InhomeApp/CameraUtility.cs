﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lamp.Plugin;

namespace InhomeApp
{
    public class CameraUtility
    {
        public void FlashTurnOn()
        {
            var v = CrossLamp.Current;
            CrossLamp.Current.TurnOn();
        }

        public void FlashTurnOff()
        {
            var v = CrossLamp.Current;
            CrossLamp.Current.TurnOff();
        }
    }
}
