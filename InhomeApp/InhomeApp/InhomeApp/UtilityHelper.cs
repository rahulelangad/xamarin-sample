﻿using Lamp.Plugin;
using Plugin.Messaging;

namespace InhomeApp
{
    public class UtilityHelper
    {
        // Make Phone Call
        public void Call(string phoneNo)
        {
            var phoneDialer = CrossMessaging.Current.PhoneDialer;
            if (phoneDialer.CanMakePhoneCall)
                phoneDialer.MakePhoneCall(phoneNo);
        }
        // Make Phone SMS
        public void Sms(string phoneNo,string smsMessage)
        {
            var smsMessenger = CrossMessaging.Current.SmsMessenger;
            if (smsMessenger.CanSendSms)
                smsMessenger.SendSms(phoneNo, smsMessage);
        }
        // Make Phone Email
        public void Email(string toEmail,string mailSubject, string mailBody)
        {
            var emailMessenger = CrossMessaging.Current.EmailMessenger;
            if (emailMessenger.CanSendEmail)
            {
                //emailMessenger.SendEmail("to.plugins@xamarin.com", "Xamarin Messaging Plugin", "Well hello there from Xam.Messaging.Plugin");
                emailMessenger.SendEmail(toEmail, mailSubject, mailBody);
                string toMailFormat = "\"to." + toEmail;
                //string ccMailFormat = "\"cc." + ccMail;
                //string bccMailFormat = "\"Bcc."+ BccMail;


                var email = new EmailMessageBuilder()
                 .To(toMailFormat)
                 //.Cc("cc.plugins@xamarin.com")
                 //.Bcc(new[] { "bcc1.plugins@xamarin.com", "bcc2.plugins@xamarin.com" })
                 .Subject(mailSubject)
                 .Body(mailBody)
                 .Build();

                emailMessenger.SendEmail(email);
            }
        }

        public void FlashTurnOn()
        {
            var v = CrossLamp.Current;
            CrossLamp.Current.TurnOn();
        }

        public void FlashTurnOff()
        {
            var v = CrossLamp.Current;
            CrossLamp.Current.TurnOff();
        }
    }
}
